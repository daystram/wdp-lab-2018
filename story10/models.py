from django.db import models

# Create your models here.
class Registrants(models.Model):
  name = models.CharField(max_length=50, blank=False, null=False)
  email = models.EmailField(max_length=50, blank=False, null=False)
  password = models.CharField(max_length=50, blank=False, null=False)
