from django import forms

class RegisterForm(forms.Form):
  name_attrs = {
    'id': 'name_field',
    'type': 'text',
    'maxlength': 50,
    'class': 'form-control',
    'placeholder': 'Name',
  }

  email_attrs = {
    'id': 'email_field',
    'type': 'email',
    'class': 'form-control',
    'placeholder': 'Email',
  }

  password_attrs = {
    'id': 'password_field',
    'type': 'password',
    'minlength': 8,
    'class': 'form-control',
    'placeholder': 'Password',
  }

  name = forms.CharField(label='', max_length=50, required=True, widget=forms.TextInput(attrs=name_attrs))
  email = forms.CharField(label='', max_length=50, required=True, widget=forms.EmailInput(attrs=email_attrs))
  password = forms.CharField(label='', min_length=8, required=True, widget=forms.PasswordInput(attrs=password_attrs))
