from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from django.test import TestCase
from django.urls import resolve
from . import views

class Story10Test(TestCase):
  def test_home_page_exists(self):
    response = self.client.get('/')
    self.assertEqual(response.status_code,200)

  def test_more_page_exists(self):
    response = self.client.get('/')
    self.assertEqual(response.status_code,200)
    
  def test_register_page_exists(self):
    response = self.client.get('/')
    self.assertEqual(response.status_code,200)

  def test_using_index_func(self):
    found = resolve('/')
    self.assertEqual(found.func, views.index)
    
  def test_using_more_func(self):
    found = resolve('/more/')
    self.assertEqual(found.func, views.more)
        
  def test_using_register_func(self):
    found = resolve('/register/')
    self.assertEqual(found.func, views.register)

  def test_using_index_template(self):
    response = self.client.get('/')
    self.assertTemplateUsed(response, 'story10/index.html')
        
  def test_using_more_template(self):
    response = self.client.get('/more/')
    self.assertTemplateUsed(response, 'story10/more.html')
        
  def test_using_register_template(self):
    response = self.client.get('/register/')
    self.assertTemplateUsed(response, 'story10/register.html')

  def test_register_api(self):
    response = self.client.post('/register/api/add_registrant/', {'name': 'John Appleseed', 'email': 'user@mail.com', 'password': 'Password1'})
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'success': 'true'})
    response = self.client.post('/register/api/add_registrant/', {'name': 'John Appleseed', 'email': 'user@mail.com', 'password': 'Password1'})
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'success': 'false', 'message': 'email already registered'})

  def test_email_check_api(self):
    response = self.client.get('/register/api/email_check/?email=user@mail.com')
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'success' : 'true', 'message': {'exists': 'false'}})
    response = self.client.post('/register/api/add_registrant/', {'name': 'John Appleseed', 'email': 'user@mail.com', 'password': 'Password1'})
    response = self.client.get('/register/api/email_check/?email=user@mail.com')
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'success' : 'true', 'message': {'exists': 'true'}})

  def test_email_regex(self):
    self.assertTrue(views.email_valid("user@mail.com"))

  def test_register_api_errors(self):
    response = self.client.post('/register/api/add_registrant/', {'name': 'John Appleseed', 'email': 'user@mail.com'})
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'error': 'true', 'message': 'invalid parameters'})
    response = self.client.post('/register/api/add_registrant/', {'name': 'John Appleseed', 'email': 'Hello!', 'password': 'Password1'})
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'error': 'true', 'message': 'invalid parameters'})
    response = self.client.get('/register/api/add_registrant/', {'name': 'John Appleseed', 'email': 'user@mail.com', 'password': 'Password1'})
    self.assertJSONEqual(str(response.content, encoding='utf8'), {'error': 'true', 'message': 'invalid request method'})

# class Story10FunctionalTest(StaticLiveServerTestCase):
#   def setUp(self):
#     self.options = webdriver.ChromeOptions()
#     self.options.add_argument('--dns-prefetch-disable')
#     self.options.add_argument('--no-sandbox')        
#     self.options.add_argument('--headless')
#     self.options.add_argument('disable-gpu')
#     self.browser = webdriver.Chrome(chrome_options=self.options)
#     self.browser.get(self.live_server_url + "/register/")
#     self.browser.implicitly_wait(8)
#     super(Story10FunctionalTest, self).setUp()

#   def tearDown(self):
#     self.browser.quit()
#     super(Story10FunctionalTest, self).tearDown()

#   def test_register_page_loads(self):
#     assert self.browser.page_source.find("REGISTER")

#   def test_register(self):
#     name_field = self.browser.find_element_by_id("name_field")
#     email_field = self.browser.find_element_by_id("email_field")
#     pass_field = self.browser.find_element_by_id("password_field")
#     ver_pass_field = self.browser.find_element_by_id("ver_password_field")
#     name_field.send_keys("John Appleseed")
#     email_field.send_keys("user@mail.com")
#     pass_field.send_keys("Password1")
#     ver_pass_field.send_keys("Password1")
#     ver_pass_field.submit()
#     self.browser.implicitly_wait(5)
#     assert self.browser.page_source.find("Registration successful!")
#     email_field.send_keys("user@mail.com")
  
#   def test_double_email(self):
#     name_field = self.browser.find_element_by_id("name_field")
#     email_field = self.browser.find_element_by_id("email_field")
#     pass_field = self.browser.find_element_by_id("password_field")
#     ver_pass_field = self.browser.find_element_by_id("ver_password_field")
#     name_field.send_keys("John Appleseed")
#     email_field.send_keys("user@mail.com")
#     pass_field.send_keys("Password1")
#     ver_pass_field.send_keys("Password1")
#     ver_pass_field.submit()
#     self.browser.implicitly_wait(5)
#     email_field.send_keys("user@mail.com")
#     name_field.send_keys("")
#     self.browser.implicitly_wait(5)
#     assert self.browser.page_source.find("This email is already registered!")

#   def test_password_no_match(self):
#     pass_field = self.browser.find_element_by_id("password_field")
#     ver_pass_field = self.browser.find_element_by_id("ver_password_field")
#     pass_field.send_keys("Password1")
#     ver_pass_field.send_keys("Password2")
#     pass_field.send_keys("")
#     assert self.browser.page_source.find("Passwords do not match!")
    
#   def test_password_short(self):
#     pass_field = self.browser.find_element_by_id("password_field")
#     ver_pass_field = self.browser.find_element_by_id("ver_password_field")
#     pass_field.send_keys("P1")
#     ver_pass_field.send_keys("")
#     assert self.browser.page_source.find("Your password has to be 8 or more characters long!")

#   def test_email_regex(self):
#     email_field = self.browser.find_element_by_id("email_field")
#     ver_pass_field = self.browser.find_element_by_id("ver_password_field")
#     email_field.send_keys("Hello!")
#     ver_pass_field.send_keys("")
#     assert self.browser.page_source.find("Plese enter a valid email!")
