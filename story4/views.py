from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'story4/index.html', {'view_name': 'index'})


def more(request):
    return render(request, 'story4/more.html', {'view_name': 'more'})


def signup(request):
    return render(request, 'story4/signup.html', {'view_name': 'signup'})

