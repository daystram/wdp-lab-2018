from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import AddSchedule
from .models import PersonalSchedule

def index(request):
    response = {
        'view_name': 'index',
    }

    return render(request, 'story5/index.html', response)


def more(request):
    response = {
        'view_name': 'more',
    }

    return render(request, 'story5/more.html', response)


def signup(request):
    response = {
        'view_name': 'signup',
    }

    return render(request, 'story5/signup.html', response)


def schedule(request):
    response = {
        'view_name': 'schedule',
        'schedule': PersonalSchedule.objects.all()
    }

    return render(request, 'story5/schedule.html', response)

def add_schedule(request):
    response = {
        'view_name': 'schedule',
        'schedule_form': AddSchedule
    }

    return render(request, 'story5/schedule-add.html', response)

def save_schedule(request):
    form = AddSchedule(request.POST)
    if (form.is_valid()):
        cleaned_data = form.cleaned_data
        if(request.method == 'POST'):
            schedule = PersonalSchedule(name=cleaned_data['name'], \
                                        date=cleaned_data['date'], \
                                        time=cleaned_data['time'], \
                                        location=cleaned_data['location'], \
                                        category=cleaned_data['category'], \
                                        desc=cleaned_data['desc'])
            schedule.save()
            return HttpResponseRedirect('/schedule/')
        else:
            response = {
                'view_name': 'schedule-add'
            }
            return render(request, 'story5/schedule-add.html', response)
    else:
        print("bad input")

def removeitem_schedule(request):
    PersonalSchedule.objects.filter(id=request.POST.get('id')).delete()
    return HttpResponseRedirect('/schedule/')

def removeall_schedule(request):
    PersonalSchedule.objects.all().delete()
    return HttpResponseRedirect('/schedule/')

