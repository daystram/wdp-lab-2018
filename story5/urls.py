from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('more/', views.more, name='more'),
    path('signup/', views.signup, name='signup'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/add/', views.add_schedule, name='schedule-add'),
    path('schedule/save/', views.save_schedule, name='schedule-save'),
    path('schedule/removeall/', views.removeall_schedule, name='schedule-removeall'),
]
