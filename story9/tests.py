from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from django.test import TestCase
from django.urls import resolve
from . import views

class Story9Test(TestCase):
  def test_home_page_exists(self):
    response = self.client.get('/')
    self.assertEqual(response.status_code,200)

  def test_using_index_func(self):
    found = resolve('/')
    self.assertEqual(found.func, views.index)

  def test_using_index_template(self):
    response = self.client.get('/')
    self.assertTemplateUsed(response, 'story9/index.html')

class Story9FunctionalTest(StaticLiveServerTestCase):
  def setUp(self):
    self.options = webdriver.ChromeOptions()
    self.options.add_argument('--dns-prefetch-disable')
    self.options.add_argument('--no-sandbox')        
    self.options.add_argument('--headless')
    self.options.add_argument('disable-gpu')
    self.browser = webdriver.Chrome(chrome_options=self.options)
    super(Story9FunctionalTest, self).setUp()

  def tearDown(self):
    self.browser.quit()
    super(Story9FunctionalTest, self).tearDown()

  def test_page_loads(self):
    self.browser.get(self.live_server_url)
    self.browser.implicitly_wait(5)
    assert self.browser.page_source.find("Go search some books!")

  def test_url_param(self):
    self.browser.get(self.live_server_url + "?q=book")
    self.browser.implicitly_wait(5)
    book_item = self.browser.find_element_by_class_name("book-item")
    assert book_item is not None

  def test_book_search(self):
    self.browser.get(self.live_server_url)
    self.browser.implicitly_wait(5)
    assert self.browser.page_source.find("Go search some books!")
    search_field = self.browser.find_element_by_id("search-field")
    search_field.send_keys("book")
    search_field.submit()
    book_item = self.browser.find_element_by_class_name("book-item")
    assert book_item is not None
  
  def test_book_not_found(self):
    self.browser.get(self.live_server_url)
    self.browser.implicitly_wait(5)
    search_field = self.browser.find_element_by_id("search-field")
    search_field.send_keys("aiwuyfgvbsdjbvcsalef")
    search_field.submit()
    assert self.browser.page_source.find("No books related")

  def test_favourite(self):
    self.browser.get(self.live_server_url + "?q=book")
    self.browser.implicitly_wait(5)
    button = self.browser.find_element_by_id("favourite-button")
    button.click()
    favourite_count = self.browser.find_element_by_class_name("favourite-count")
    assert favourite_count.text == "1"
    button.click()
    assert favourite_count.text == "0"
