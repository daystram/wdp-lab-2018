from django.db import models
from django.utils import timezone

class Status(models.Model):
    text = models.TextField();
    creation = models.DateTimeField(default=timezone.now)
