from django import forms

class StatusForm(forms.Form):
    error_message = {
        'required': 'Please give input'
    }

    text_attrs = {
        'id': 'input-field',
        'type': 'text',
        'placeholder': 'How are you?',
        'class': 'form-control'
    }

    text = forms.CharField(required=True, label='', widget=forms.TextInput(attrs=text_attrs))
    