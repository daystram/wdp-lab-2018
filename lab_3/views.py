from django.shortcuts import render

def index(request):
    return render(request, 'lab_3/to_do_list.html')
